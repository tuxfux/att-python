#!/usr/bin/python
import string
#my_string = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
my_string='map'
my_alpha=list(string.letters[:25])
my_output = []

# main
for value in my_string:
  if value in my_alpha:
    if value == 'y':
      my_output.append('a')
    elif value == 'z':
      my_output.append('b')
    else:
      my_output.append(my_alpha[my_alpha.index(value) + 2])
  else:
    my_output.append(value)

print "my old string {}".format(my_string) 
print 'my new string is {}'.format("".join(my_output))
