# tuples
# tuples are read only lists.

In [1]: my_gender = ('male','female')

In [2]: print my_gender,type(my_gender)
('male', 'female') <type 'tuple'>

In [3]: my_empty = ()

In [4]: print my_empty,type(my_empty)
() <type 'tuple'>

In [5]: my_empty = tuple()

In [6]: print my_empty,type(my_empty)
() <type 'tuple'>

In [7]: # tuples

In [8]: # indexing,slicing

In [10]: my_gender
Out[10]: ('male', 'female')

In [11]: my_gender[0]
Out[11]: 'male'

In [12]: my_gender[0] = 'Male'
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-12-6d7934be9bc2> in <module>()
----> 1 my_gender[0] = 'Male'

TypeError: 'tuple' object does not support item assignment

In [13]: # packing and unpacking

In [14]: my_fruits = ('apple','banana','cherry','dates')

In [15]: #ex: a = my_fruits[0],b=my_fruits[1],c=my_fruits[2]

In [16]: a,b,c,d = my_fruits

In [17]: print a
apple

In [18]: print b
banana

In [19]: print c
cherry

In [20]: print d
dates

In [21]: x,y,z = my_fruits
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-21-c71dcc90520c> in <module>()
----> 1 x,y,z = my_fruits

ValueError: too many values to unpack

In [22]: u,v,w,x,y = my_fruits
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-22-d6d3c0bb92d0> in <module>()
----> 1 u,v,w,x,y = my_fruits

ValueError: need more than 4 values to unpack

In [24]: # lists and tuples

In [25]: my_students = ['rupesh','praveen','abhi','daya','sukanta']

In [26]: my_subjects = ['vmware','python','devops','PMP','aws']

In [27]: name='rupesh'

In [28]: name in my_students # hall ticket
Out[28]: True

In [29]: my_students.index(name)
Out[29]: 0

In [30]: my_subjects[my_students.index(name)]
Out[30]: 'vmware'

In [31]: if name in my_students:
    ...:     print "{} is going to give exam {}".format(name,my_subjects[my_students.index(name)])
    ...:
rupesh is going to give exam vmware

In [32]: name='daya'

In [33]: if name in my_students:
    ...:     print "{} is going to give exam {}".format(name,my_subjects[my_students.index(name)])
    ...:
daya is going to give exam PMP

In [34]:

In [34]: # Anti climax

In [35]: my_students.sort()

In [36]: print my_students
['abhi', 'daya', 'praveen', 'rupesh', 'sukanta']

In [37]: print my_subjects
['vmware', 'python', 'devops', 'PMP', 'aws']

In [38]: name='abhi'

In [39]: if name in my_students:
    ...:     print "{} is going to give exam {}".format(name,my_subjects[my_students.index(name)])
    ...:
abhi is going to give exam vmware

In [40]:

In [40]: name='daya'

In [41]: if name in my_students:
    ...:     print "{} is going to give exam {}".format(name,my_subjects[my_students.index(name)])
    ...:
daya is going to give exam python

In [42]:

In [43]: my_students = ['rupesh','praveen','abhi','daya','sukanta']

In [44]: my_subjects = ['vmware','python','devops','PMP','aws']

In [45]: my_exams = [('rupesh','vmware'),('praveen','python'),('abhi','devops'),('daya','PMP'),('sukanta
    ...: ','aws')]

In [46]: print my_exams
[('rupesh', 'vmware'), ('praveen', 'python'), ('abhi', 'devops'), ('daya', 'PMP'), ('sukanta', 'aws')]

In [47]: my_exams[0]
Out[47]: ('rupesh', 'vmware')

In [48]: my_exams[0][0]
Out[48]: 'rupesh'

In [49]: my_exams[0][0]='Rupesh'
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-49-728e96791b71> in <module>()
----> 1 my_exams[0][0]='Rupesh'

TypeError: 'tuple' object does not support item assignment

In [50]: my_exams[0] = ('rupesh','citrix')

In [51]: print my_exams
[('rupesh', 'citrix'), ('praveen', 'python'), ('abhi', 'devops'), ('daya', 'PMP'), ('sukanta', 'aws')]

In [52]:

In [53]: my_exams = [('rupesh','vmware'),('praveen','python'),('abhi','devops'),('daya','PMP'),('sukanta
    ...: ','aws')]

In [54]: name='sukanta'

In [55]: for value in my_exams:
    ...:     print value
    ...:
('rupesh', 'vmware')
('praveen', 'python')
('abhi', 'devops')
('daya', 'PMP')
('sukanta', 'aws')

In [56]: for student,subject in my_exams:
    ...:     print student,subject
    ...:
rupesh vmware
praveen python
abhi devops
daya PMP
sukanta aws

In [57]: for student,subject in my_exams:
    ...:     if name == student:
    ...:         print "{} is going give exam {}".format(student,subject)
    ...:
sukanta is going give exam aws

In [58]: # anticlimax

In [59]: my_exams.sort()

In [60]: print my_exams
[('abhi', 'devops'), ('daya', 'PMP'), ('praveen', 'python'), ('rupesh', 'vmware'), ('sukanta', 'aws')]

In [61]: for student,subject in my_exams:
    ...:     if name == student:
    ...:         print "{} is going give exam {}".format(student,subject)

In [63]: # functions

In [64]: my_gender.count?
Docstring: T.count(value) -> integer -- return number of occurrences of value
Type:      builtin_function_or_method

In [65]: my_gender.index?
Docstring:
T.index(value, [start, [stop]]) -> integer -- return first index of value.
Raises ValueError if the value is not present.
Type:      builtin_function_or_method

In [66]: my_gender.count('male')
Out[66]: 1

In [67]: my_gender.index('female')
Out[67]: 1

In [68]: my_gender.index('male')
Out[68]: 0

In [69]:


