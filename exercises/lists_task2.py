#!/usr/bin/python
days = ['yesterday','today','tomorrow','dayafter']
for value in days:
  print value[:days.index(value) + 1].upper() + value[days.index(value) + 1:]


'''
In [62]: days = ['yesterday','today','tomorrow','dayafter']

In [63]: for value in days:
    ...:     print value
    ...:
yesterday
today
tomorrow
dayafter

In [64]: for value in days:
    ...:     print value,days.index(value)
    ...:
yesterday 0
today 1
tomorrow 2
dayafter 3

In [65]: for value in days:
    ...:     print value,days.index(value),value[days.index(value)]
    ...:
    ...:
yesterday 0 y
today 1 o
tomorrow 2 m
dayafter 3 a

In [66]: for value in days:
    ...:     print "{}-{}-{}-{}".format(value,days.index(value),value[days.index(value)],value[0:days.index(value)])
    ...:
yesterday-0-y-
today-1-o-t
tomorrow-2-m-to
dayafter-3-a-day

In [67]: my_string="python"

In [68]: my_string[0:3]
Out[68]: 'pyt'

In [69]: my_string[0:0]
Out[69]: ''

In [70]: for value in days:
    ...:     print "{}-{}-{}-{}".format(value,days.index(value),value[days.index(value)],value[0:days.index(value) + 1])
    ...:
    ...:
yesterday-0-y-y
today-1-o-to
tomorrow-2-m-tom
dayafter-3-a-daya

In [71]:

In [71]: for value in days:
    ...:     print value[:days.index(value) + 1] + value[days.index(value) + 1]
    ...:
    ...:
    ...:
ye
tod
tomo
dayaf

In [72]: for value in days:
    ...:     print value[:days.index(value) + 1] + value[days.index(value) + 1:]
    ...:
yesterday
today
tomorrow
dayafter

In [73]: for value in days:
    ...:     print value[:days.index(value) + 1].upper() + value[days.index(value) + 1:]
    ...:
    ...:
Yesterday
TOday
TOMorrow
DAYAfter

In [74]:

In [74]: my_string="python"

In [75]: my_string[0:3
    ...: ]
Out[75]: 'pyt'

In [76]: my_string[:3]
Out[76]: 'pyt'

In [77]: my_string[3:]
Out[77]: 'hon'

In [78]: my_string[:3] + my_string[3:]
Out[78]: 'python'

'''
