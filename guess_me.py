#!/usr/bin/python
# break : A sane way of coming out of a loop.(for/while)
# task: max chances to be given is only 3 times.

import sys

ans = raw_input("do you want to play the game - y/n:")
if ans == 'n':
  sys.exit()

my_num = 7
#test = True

#while test:   # while loop always works for truth of a condition
while True:
  guess_num = input("please enter your number:")
  if guess_num > my_num:
    print "the number you guessed is slightly larger"
  elif guess_num < my_num:
    print "the number you guessed is slightly smaller"
  elif guess_num == my_num:
    print "congo!!! you guessed the right number"
    #test = False
    break

print "End of the game."
